# translation of kcmtelepathyaccounts_plugin_salut.po to Français
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Joëlle Cornavin <jcorn@free.fr>, 2011.
# xavier <xavier.besnard@neuf.fr>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: kcmtelepathyaccounts_plugin_salut\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2019-05-20 03:11+0200\n"
"PO-Revision-Date: 2013-11-30 16:18+0100\n"
"Last-Translator: Maxime Corteel\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. i18n: ectx: property (title), widget (QGroupBox, additionalGroupBox)
#: salut-advanced-options-widget.ui:17
#, kde-format
msgid "Additional Information"
msgstr "Informations complémentaires"

#. i18n: ectx: property (text), widget (QLabel, pubnameLabel)
#: salut-advanced-options-widget.ui:26
#, kde-format
msgid "Published name:"
msgstr "Nom publié :"

#. i18n: ectx: property (text), widget (QLabel, emailLabel)
#: salut-advanced-options-widget.ui:40
#, kde-format
msgid "Email:"
msgstr "Courrier électronique :"

#. i18n: ectx: property (text), widget (QLabel, jidLabel)
#: salut-advanced-options-widget.ui:54
#, kde-format
msgid "Jabber ID:"
msgstr "ID Jabber :"

#. i18n: ectx: property (windowTitle), widget (QWidget, SalutMainOptionsWidget)
#: salut-main-options-widget.ui:14
#, kde-format
msgid "Account Preferences"
msgstr "Préférences de compte"

#. i18n: ectx: property (text), widget (QLabel, firstnameLabel)
#: salut-main-options-widget.ui:25
#, kde-format
msgid "First name:"
msgstr "Prénom :"

#. i18n: ectx: property (text), widget (QLabel, lastnameLabel)
#: salut-main-options-widget.ui:38
#, kde-format
msgid "Last name:"
msgstr "Nom de famille :"

#. i18n: ectx: property (text), widget (QLabel, nicknameLabel)
#: salut-main-options-widget.ui:51
#, kde-format
msgid "Nickname:"
msgstr "Pseudonyme :"
