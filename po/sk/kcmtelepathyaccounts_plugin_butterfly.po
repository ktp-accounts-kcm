# translation of kcmtelepathyaccounts_plugin_butterfly.po to Slovak
# Richard Frič <Richard.Fric@kdemail.net>, 2012.
# Roman Paholik <wizzardsk@gmail.com>, 2014, 2015.
msgid ""
msgstr ""
"Project-Id-Version: kcmtelepathyaccounts_plugin_butterfly\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2019-05-20 03:11+0200\n"
"PO-Revision-Date: 2015-08-01 12:29+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#. i18n: ectx: property (windowTitle), widget (QWidget, MainOptionsWidget)
#: main-options-widget.ui:20
#, kde-format
msgid "Account Preferences"
msgstr "Predvoľby účtu"

#. i18n: ectx: property (text), widget (QLabel, accountLabel)
#: main-options-widget.ui:29
#, kde-format
msgid "Email address:"
msgstr "E-mailová adresa:"

#. i18n: ectx: property (text), widget (QLabel, label)
#: main-options-widget.ui:46
#, kde-format
msgid "Example: user@hotmail.com"
msgstr "Príklad: user@hotmail.com"

#. i18n: ectx: property (text), widget (QLabel, passwordLabel)
#: main-options-widget.ui:53
#, kde-format
msgid "Password:"
msgstr "Heslo:"

#~ msgctxt "name of the IM service Telegram"
#~ msgid "Telegram (%1)"
#~ msgstr "Telegram (%1)"

#~ msgid "An authentication code will be texted to your phone on first login."
#~ msgstr "Kód overenia bude odoslaný na váš mobil pri prvom prihlásení."

#~ msgid "Phone Number:"
#~ msgstr "Telefónne číslo:"
