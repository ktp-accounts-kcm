# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Giovanni Sora <g.sora@tiscali.it>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2019-05-20 03:11+0200\n"
"PO-Revision-Date: 2013-02-27 14:53+0100\n"
"Last-Translator: Giovanni Sora <g.sora@tiscali.it>\n"
"Language-Team: Interlingua <kde-l10n-ia@kde.org>\n"
"Language: ia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 1.4\n"

#. i18n: ectx: property (windowTitle), widget (QWidget, MainOptionsWidget)
#: main-options-widget.ui:20
#, kde-format
msgid "Account Preferences"
msgstr "Preferentias de conto"

#. i18n: ectx: property (text), widget (QLabel, accountLabel)
#: main-options-widget.ui:29
#, kde-format
msgid "Email address:"
msgstr "Adresse de E-Posta:"

#. i18n: ectx: property (text), widget (QLabel, label)
#: main-options-widget.ui:46
#, kde-format
msgid "Example: user@hotmail.com"
msgstr "Exemplo: usator@hotmail.com"

#. i18n: ectx: property (text), widget (QLabel, passwordLabel)
#: main-options-widget.ui:53
#, kde-format
msgid "Password:"
msgstr "Contrasigno:"
